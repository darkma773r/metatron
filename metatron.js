/**
* Metatron - Version 1.1 
*	
* Tool for describing and manipulating meta data on plain JavaScript objects.
*
* Matt Juntunen, 2013
*/
(function(window){

var EMPTY_STRING_REGEX = /^\s*$/;
var ARRAY_META_PROPERTY_REGEX = /^__\d+$/;
var ARRAY_PATH_REGEX = /^\w+\[\]$/;
var ARRAY_INDEX_REGEX = /^\d+$/;

function isObject(val){
	return !isArray(val) && typeof val === metatron.OBJECT;
}

function isArray(val){
	return val instanceof Array;
}

function isUndefined(val){
	return val === undefined;
}

function isNull(val){
	return val === null;
}

function isString(val){
	return typeof val === metatron.STRING;
}

function hasValue(val){
	return !isUndefined(val) && !isNull(val);
}

/**
Returns a clone of val.
*/
function clone(val){
	if (!hasValue(val)){
		return val;
	}

	return JSON.parse(JSON.stringify(val));
}

/**
Takes a path in the form of a string or array and returns an array representing the path.
*/
function readPath(path){
	if (!hasValue(path)){	
		return [];
	} else if (isArray(path)){
		return path.slice(0);
	} else if (isString(path)){
		if (EMPTY_STRING_REGEX.test(path)){
			return []; //return an empty array for empty strings
		}
		return path.split('.');
	} else {
		return [path + '']; //convert to string
	}
}

function getType(val){
	if (isArray(val)){
		return metatron.ARRAY;
	}
	return typeof val;
}

function isFunction(val){
	return typeof val === metatron.FUNCTION;
}

function foreach(obj, callback){
	if (obj === undefined || obj === null){
		return;
	}

	if (isArray(obj)){
		for (var i=0; i<obj.length; i++){
			callback(i, obj);
		}
	} else if (isObject(obj)){
		for (var prop in obj){
			if (obj.hasOwnProperty(prop) && !isFunction(obj[prop])){
				callback(prop, obj);
			}
		}
	}
}

function propertyNamesMatch(obj, regex){
	for (var prop in obj){
		if (obj.hasOwnProperty(prop) && !regex.test(prop)){
			return false;
		}
	}

	return true;
}

function hasArrayMetadataObject(arr){
	if (arr && arr.length > 0){
		var first = arr[0];
		if (hasValue(first) && isObject(first) && propertyNamesMatch(first, ARRAY_META_PROPERTY_REGEX)){
			return true;
		}
	}

	return false;
}

/**
Primary  Metatron function. Creates a MetatronDescriptor object describing val. If useMeta is true or not given, properties
beginning with double underscores or objects occurring first in arrays and which consist entirely of properties beginning with
double underscores are considered to contain meta data to be attached to other properties. If useMeta is false, then these
objects and properties are interpreted exactly as-is.
*/
metatron = function(val, useMeta){
	useMeta = useMeta !== false; //useMeta default is true
	
	if (val instanceof MetatronDescriptor){
		val = val.build(true);
	}
	
	return new MetatronDescriptor(val, null, null, useMeta); 
};

/**
Constructor for MetatronDescriptor objects
*/
function MetatronDescriptor(val, name, parent, useMeta, metaData){
	var self = this;
	this.type = getType(val);
	
	if (isObject(val) || isArray(val)){
		this.value = null; //objects and arrays are containers, they don't contain values themselves
	} else {
		this.value = val;
	}
	
	this.isNull = isNull(val);
	this.isUndefined = isUndefined(val);
	
	this.name = hasValue(name)? name + '' : null; //force conversion to string

	this.parent = parent;
	
	//determine the path to this object
	this.path = [];
	var curDescriptor = this;
	while (curDescriptor && hasValue(curDescriptor.name)){
		// in the path array, add '[]' to the end of array names
		var curName = curDescriptor.type === metatron.ARRAY ? 
			curDescriptor.name + '[]' 
			: curDescriptor.name;
		this.path.unshift(curName);
		
		curDescriptor = curDescriptor.parent;
	}
	this.pathString = this.path.join('.');
	
	//populate children
	if (hasValue(val) && (isObject(val) || isArray(val))){
		this.children = [];
		
		if (useMeta){
			//find a source for child metadata information; for objects this is the object itself. For
			//arrays, this will be an object that is the first element in the array that has properties with names of the form
			// '__<number>'. The meta data is then tied to the elements in the array using the indices as names.
			var metadataSource, 
				skipFirstElement;
			
			if (isArray(val) && hasArrayMetadataObject(val)){
				metadataSource = val[0];
				skipFirstElement = true;
			} else {
				metadataSource = val;
				skipFirstElement = false;
			}

			foreach(val , function(i, parent){
				if ((i + '').indexOf('__') !== 0 && !(skipFirstElement && i === 0)){ //skip meta data items
					var adjustedMetadataIndex = skipFirstElement ? i - 1 : i; //adjust for any skipped array items
					self.children.push(
						new MetatronDescriptor(parent[i], adjustedMetadataIndex, self, useMeta, metadataSource['__' + adjustedMetadataIndex]));
				} 
			});
		} else { //no meta data
			foreach(val, function(i, parent){
				self.children.push(new MetatronDescriptor(parent[i], i, self, useMeta, null));
			});
		}
	} else { //no children
		this.children = undefined; //set to undefined for when this function is called to modify an existing object
	}
	
	//add meta data if enabled and present
	if (useMeta && hasValue(metaData)){
		this.meta = clone(metaData);
	} else {
		this.meta = undefined; //set to undefined for when this function is called to modify an existing object
	}
}

MetatronDescriptor.prototype = {
	constructor : metatron, 
	
	/**
	Searches this descriptor's children for the object matching the given path. Path can be an array of strings
	or a single, period-delimited path string, such as "root.subobject.property". This function is only useful
	for descriptors with type object or array. Null is returned in the descriptor at the given path is not found.
	*/
	find: function(path){	
		//convert the path to an array
		var pathArr = readPath(path);
		
		if (pathArr.length < 1){
			return this; //we're the last object in the path chain so we should just return ourselves here
		}
		
		var nameToFind = pathArr.shift() + ''; //shift and force conversion to string
		var isArrayName = ARRAY_PATH_REGEX.test(nameToFind);
		if (isArrayName){
			nameToFind = nameToFind.replace('[]', '');
		}
		
		//start searching through children
		if (this.children){
			for (var i=0; i<this.children.length; i++){
				var child = this.children[i];
				if (child && child.name == nameToFind){ //we found a match!
					return child.find(pathArr); //let the child find the rest
				}
			}
		}
		
		return null; //not found
	},
	
	/**
	Sets the value of this descriptor object to value. If useMeta is false, then any meta data properties in the value are
	interpreted as-is. The name, parent, and meta data fields are carried over. This is similar to the metatron() function
	but for use with already existing descriptors, including ones that are within descriptor trees.
	*/
	set: function(val, useMeta){
		//default for useMeta is true
		useMeta = useMeta !== false;

		if (val instanceof MetatronDescriptor){
			val = val.build(true);
		} 
			
		MetatronDescriptor.call(this, val, this.name, this.parent, useMeta, this.meta);
		
		return this;
	},
	
	/**
	Sets descriptor node at path to val, skipping meta data interpretation if useMeta is false. Portions of the path that do not
	exist are created and any descriptors in the path that need to be converted into containers are modified. For example, if a path element
	is a numeric index and the parent descriptor object is not an array, it is converted into an array, losing all other child objects if
	any exist. Similarly, if a path element is a non-integer string and the parent descriptor is not an object, it is converted to an object
	before the new child is added. Parent relationships and meta information are retained.
	*/
	setPath: function(path, val, useMeta){
		//convert the path to an array
		var pathArr = readPath(path);
		
		if (pathArr.length < 1){
			//this is the descriptor we're looking for
			return this.set(val, useMeta);
		}
		
		//find the object that we're modifying
		var nameToFind = pathArr.shift() + ''; //shift and force conversion to string
		var target = this.find(nameToFind);
		
		if (!target) {
			//the target did not exist, we're going to need to create it
			
			//make sure we're the right type of container
			if (ARRAY_INDEX_REGEX.test(nameToFind)) { 
				if (this.type !== metatron.ARRAY){
					this.set([]); //switch type to array
				}
			} else if (this.type !== metatron.OBJECT){
				this.set({}); //switch type to object
			}
			
			this.children = this.children || [];
			
			//clean the target name
			nameToFind = nameToFind.replace('[]', '');

			//create a new, blank child
			target = new MetatronDescriptor(undefined, nameToFind, this);
			this.children.push(target);
		}
		
		//call the next level recusively
		target.setPath(pathArr, val, useMeta);
		
		return this; //each level returns itself; this means that ultimately the original node will be returned to the caller
	},
	
	/**
	Removes this descriptor from its parent, if it has one. Otherwise, does nothing.
	*/
	remove: function(){
		//remove our parent's reference to us, if needed
		if (this.parent && this.parent.children){
		
			var idx = -1;
		
			for (var i=0; i<this.parent.children.length; i++){
				if (this.parent.children[i] === this){
					idx = i;
					break;
				}
			}
			
			if (idx > -1){
				this.parent.children.splice(idx, 1);
			}
		}
		
		//remove our reference to our parent
		this.parent = null;
	
		return this;
	},
	
	/**
	Removes the descriptor at the given path if it exists. This method simply calls find(), and then remove() on the returned
	object, if any. The removed node is returned if found.
	*/
	removePath: function(path){
		var node = this.find(path);
		if (node){
			node.remove();
		}
		
		return node;
	},
	
	/**
	Iterates through all children, calling fn on each in the descriptor's context (i.e. the 'this' variable refers to the child descriptor). The
	descriptor and all its children are removed if the function returns a falsy value, otherwise there is no effect.
	*/
	filter: function(fn){
		if (this.children){
			var childrenArray = this.children.slice(0); //make a copy of the array so we're not iterating over the array we're changing
			
			for (var i=0; i<childrenArray.length; i++){
				var child = childrenArray[i];

				if (fn.apply(child)){
					child.filter(fn); //filter the child's children
				} else {
					child.remove(); //remove the child
				}
			}
		}
	},
	
	/**
	Convenience method for accessing meta data values without needing null checks on the meta property. If the meta property doesn't
	have any value when this method is called, then an empty object is assigned to it and returned.
	*/
	getMeta: function(){
		if (!hasValue(this.meta)){
			this.meta = {};
		}
		return this.meta;
	},
	
	/**
	Returns a deep copy of the original object. If includeMeta is true, then the meta data for the object is
	included in the return value. If false or not given, the meta data is excluded.
	*/
	build: function(includeMeta){
		if (this.type === metatron.OBJECT || this.type === metatron.ARRAY){ //this is a container object
			if (this.isNull){
				return null;
			}
	
			var hasMeta = false;
		
			//build the result and meta objects
			if (this.type === metatron.OBJECT){
				var result = {};
				var metaObj = result; //meta object is the exact same as the container
			} else {
				var result = [];
				var metaObj = {}; //meta object will be an object at index 0 in the array
			}
			
			//go through each child object
			for (var i=0; i<this.children.length; i++){
				var child = this.children[i];
				
				//add meta if needed
				if (includeMeta && hasValue(child.meta)){
					hasMeta = true;
					metaObj['__' + child.name] = clone(child.meta);
				}
				
				//add the value of the child
				result[child.name] = child.build(includeMeta);
			}
			
			//if we have an array with a metadata object, push it to the front of the array
			if (hasMeta && this.type === metatron.ARRAY){
				result.unshift(metaObj);
			}
			
			//return the result object
			return result;
		}
		return this.value;
	}
};

/**
Type string constants
*/
metatron.NUMBER = 'number';
metatron.STRING = 'string';
metatron.BOOLEAN = 'boolean';
metatron.OBJECT = 'object';
metatron.ARRAY = 'array';
metatron.FUNCTION = 'function';
metatron.UNDEFINED = 'undefined';

/**
Expose the prototype so others can access it
*/
metatron.prototype = MetatronDescriptor.prototype;

/**
Set the top-level metatron reference
*/
window.metatron = metatron;

})(window);
/**
Metatron Unit tests.
*/

/*********** Helper functions **************/
function hasValue(x){
	return typeof x !== 'undefined' && x !== null;
}

function exists(x, msg){
	ok(hasValue(x, msg));
};

function notExists(x, msg){
	ok(!hasValue(x, msg));
};

/************** Unit Tests ********************/
test('metatron object created', function(){
	//act/assert
	ok(window.metatron);
});

test('metatron - undefined', function(){
	//act
	var desc = metatron();

	//assert
	exists(desc);
	strictEqual(desc.type, 'undefined');
	strictEqual(desc.isUndefined, true);
	strictEqual(desc.isNull, false);
	notExists(desc.value);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});


test('metatron - null', function(){
	//act
	var desc = metatron(null);

	//assert
	exists(desc);
	strictEqual(desc.type, 'object');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, true);
	notExists(desc.value);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});

test('metatron - number', function(){
	//act
	var desc = metatron(1);

	//assert
	exists(desc);
	strictEqual(desc.type, 'number');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, false);
	strictEqual(desc.value, 1);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});

test('metatron - boolean', function(){
	//act
	var desc = metatron(true);

	//assert
	exists(desc);
	strictEqual(desc.type, 'boolean');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, false);
	strictEqual(desc.value, true);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});

test('metatron - string', function(){
	//act
	var desc = metatron('hi');

	//assert
	exists(desc);
	strictEqual(desc.type, 'string');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, false);
	strictEqual(desc.value, 'hi');
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});

test('metatron - object', function(){
	//arrange
	var obj = {a: 1, b: 'hi', c: true};
	
	//act
	var desc = metatron(obj);
	
	//assert
	exists(desc);
	strictEqual(desc.type, 'object');
	notExists(desc.name);
	strictEqual(desc.children.length, 3);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.parent);
	
	var child1 = desc.children[0];
	
	strictEqual(child1.type, 'number');
	strictEqual(child1.name, 'a');
	strictEqual(child1.value, 1);
	deepEqual(child1.path, ['a']);
	notExists(child1.children);
	strictEqual(child1.parent, desc);
	
	var child2 = desc.children[1];
	
	strictEqual(child2.type, 'string');
	strictEqual(child2.name, 'b');
	strictEqual(child2.value, 'hi');
	deepEqual(child2.path, ['b']);
	notExists(child2.children);
	strictEqual(child2.parent, desc);
	
	var child3 = desc.children[2];
	
	strictEqual(child3.type, 'boolean');
	strictEqual(child3.name, 'c');
	strictEqual(child3.value, true);
	deepEqual(child3.path, ['c']);
	notExists(child3.children);
	strictEqual(child3.parent, desc);
});

test('metatron - nested object', function(){
	//arrange
	var obj = {
		child: {
			grandChild: 1,
			grandChildArr: ['greatGrandChild', false]
		}
	};

	//act
	var desc = metatron(obj, false);
	
	//assert
	
	//check the root object
	exists(desc);
	strictEqual(desc.type, 'object');
	notExists(desc.name);
	strictEqual(desc.children.length, 1);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	
	//check first level child
	var child= desc.children[0];
	
	strictEqual(child.type, 'object');
	strictEqual(child.name, 'child');
	notExists(child.value);
	deepEqual(child.path, ['child']);
	strictEqual(child.children.length, 2);
	strictEqual(child.parent, desc);
	
	//check second level first child
	var grandChild1= child.children[0];
	
	strictEqual(grandChild1.type, 'number');
	strictEqual(grandChild1.name, 'grandChild');
	strictEqual(grandChild1.value, 1);
	deepEqual(grandChild1.path, ['child', 'grandChild']);
	notExists(grandChild1.children);
	strictEqual(grandChild1.parent, child);
	
	//check second level second child
	var grandChild2= child.children[1];
	
	strictEqual(grandChild2.type, 'array');
	strictEqual(grandChild2.name, 'grandChildArr');
	notExists(grandChild2.value);
	deepEqual(grandChild2.path, ['child', 'grandChildArr[]']);
	strictEqual(grandChild2.children.length, 2);
	strictEqual(grandChild2.parent, child);
	
	//check the third level first child
	var greatGrandChild1 = grandChild2.children[0];
	
	strictEqual(greatGrandChild1.type, 'string');
	strictEqual(greatGrandChild1.name, '0');
	strictEqual(greatGrandChild1.value, 'greatGrandChild');
	deepEqual(greatGrandChild1.path, ['child', 'grandChildArr[]', '0']);
	notExists(greatGrandChild1.children);
	strictEqual(greatGrandChild1.parent, grandChild2);
	
	//check the third level second child
	var greatGrandChild2 = grandChild2.children[1];
	
	strictEqual(greatGrandChild2.type, 'boolean');
	strictEqual(greatGrandChild2.name, '1');
	strictEqual(greatGrandChild2.value, false);
	deepEqual(greatGrandChild2.path, ['child', 'grandChildArr[]', '1']);
	notExists(greatGrandChild2.children);
	strictEqual(greatGrandChild2.parent, grandChild2);
});

test('metatron - object with metadata, useMeta is true', function(){
	//arrange
	var obj = {
		__a : { metaName : 'metaValue' },
		a : true
	};
	
	//act
	var desc = metatron(obj, true);
	
	//assert
	strictEqual(desc.children.length, 1);
	
	var a = desc.children[0];
	strictEqual(a.name, 'a');
	strictEqual(a.meta.metaName, 'metaValue');
});

test('metatron - object with metadata, useMeta is true, meta value describes non-existent property', function(){
	//arrange
	var obj = {
		__x : { metaName : 'metaValue' },
		a : true
	};
	
	//act
	var desc = metatron(obj, true);
	
	//assert
	strictEqual(desc.children.length, 1);
	
	var a = desc.children[0];
	strictEqual(a.name, 'a');
	notExists(a.metaName);
});

test('metatron - object with metadata, useMeta is false', function(){
	//arrange
	var obj = {
		__a : { metaName : 'metaValue' },
		a : true
	};
	
	//act
	var desc = metatron(obj, false);
	
	//assert
	strictEqual(desc.children.length, 2);
	strictEqual(desc.children[0].name, '__a');
	strictEqual(desc.children[1].name, 'a');
});

test('metatron - array', function(){
	//act
	var desc = metatron([1, 'hi', true]);

	//assert
	exists(desc);
	strictEqual(desc.type, 'array');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, false);
	notExists(desc.value);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	exists(desc.children);
	strictEqual(desc.children.length, 3);
	
	var child1 = desc.children[0];
	
	strictEqual(child1.type, 'number');
	strictEqual(child1.value, 1);
	strictEqual(child1.parent, desc);
	deepEqual(child1.path, ['0']);
	strictEqual(child1.pathString, '0');
	
	var child2 = desc.children[1];
	
	strictEqual(child2.type, 'string');
	strictEqual(child2.value, 'hi');
	strictEqual(child2.parent, desc);
	deepEqual(child2.path, ['1']);
	strictEqual(child2.pathString, '1');
	
	var child3 = desc.children[2];
	
	strictEqual(child3.type, 'boolean');
	strictEqual(child3.value, true);
	strictEqual(child3.parent, desc);
	deepEqual(child3.path, ['2']);
	strictEqual(child3.pathString, '2');
});

test('metatron - nested array', function(){
	//act
	var node1 = metatron([[1]]);

	//assert
	strictEqual(node1.type, 'array');
	exists(node1.children);
	strictEqual(node1.children.length, 1);
	notExists(node1.parent);
	
	var node2 = node1.children[0];
	strictEqual(node2.type, 'array');
	exists(node2.children);
	strictEqual(node2.children.length, 1);
	strictEqual(node2.parent, node1);
	
	var node3 = node2.children[0];
	strictEqual(node3.type, 'number');
	strictEqual(node3.value, 1);
	strictEqual(node3.parent, node2);
});

test('metatron - array metadata', function(){
	//arrange
	var obj = [
			{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}}, //meta object
			{a: 2},
			{b: 4},
			[1,2,3]
		];
	
	//act
	var desc = metatron(obj, true);
	
	//assert
	exists(desc);
	equal(desc.children.length, 3);

	var firstElement = desc.find('0');
	exists(firstElement);
	equal(firstElement.meta.testMeta, 12);
	notExists(firstElement.meta.otherMeta);

	var secondElement = desc.find('1');
	exists(secondElement);
	notExists(secondElement.meta);
	
	var thirdElement = desc.find('2');
	exists(thirdElement);
	equal(thirdElement.meta.otherMeta, 'metaValue');
	notExists(thirdElement.meta.testMeta);
});

test('metatron - array metadata, describes non-existent index', function(){
	//arrange
	var obj = [
			{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}}, //meta object
			{a: 2}
		];
	
	//act
	var desc = metatron(obj, true);
	
	//assert
	exists(desc);
	equal(desc.children.length, 1);

	var firstElement = desc.children[0];
	exists(firstElement);
	equal(firstElement.meta.testMeta, 12);
	notExists(firstElement.meta.otherMeta);
});

test('metatron - array metadata object contains invalid property, meta data not used', function(){
	//arrange
	var obj = [
			{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}, invalid: true}, //the presence of this invalid property makes the whole meta object invalid
			{a: 2},
			{b: 4},
			[1,2,3]
		];
		
	//act	
	var desc = metatron(obj, true);
	
	//assert
	exists(desc);
	equal(desc.children.length, 4);
});

test('metatron - array metadata exists, useMeta is false', function(){
	//arrange
	var obj = [
			{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}}, //meta object
			{a: 2},
			{b: 4},
			[1,2,3]
		];
		
	//act	
	var desc = metatron(obj, false);
	
	//assert
	exists(desc);
	equal(desc.children.length, 4);
});

test('metatron - array metadata leaves original object unchanged', function(){
	//arrange
	var obj = [
		{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}}, //meta object
		{a: 2},
		{b: 4},
		[1,2,3]
	];
		
	//act
	metatron(obj, true);
	metatron(obj, true);
		
	//assert
	equal(obj.length, 4);
});

test('metatron - useMeta default value is true', function(){
	//arrange
	var obj = {__a : {x:1}, a: 2};
	
	//act
	var desc = metatron(obj);
	
	//assert
	strictEqual(desc.children.length, 1);
});

test('metatron - describing MetatronDescriptor object returns copy of descriptor', function(){
	//arrange
	var obj = {__a : {x:1}, a: 2};
	
	//act
	var desc1 = metatron(obj);
	var desc2 = metatron(desc1);  
	
	//assert
	ok(desc1 !== desc2);
	deepEqual(desc1, desc2);
});

test('metatron - describing MetatronDescriptor object returns copy of descriptor, original object is undefined', function(){
	//arrange
	var obj = undefined;
	
	//act
	var desc1 = metatron(obj);
	var desc2 = metatron(desc1);  
	
	//assert
	ok(desc1 !== desc2);
	deepEqual(desc1, desc2);
});

test('metatron - describing MetatronDescriptor object returns copy of descriptor, useMeta is false', function(){
	//arrange
	var obj = {__a : {x:1}, a: 2};
	
	//act
	var desc1 = metatron(obj, true);
	var desc2 = metatron(desc1, false);  
	
	//assert
	ok(desc1 !== desc2);
	notEqual(desc1, desc2);
	
	notExists(desc2.find('a').meta);
});

test('metatron - modifying descriptor does not affect original object', function(){
	//arrange
	var obj = { arr: [1, {x: 'goal'}]};
	
	//act
	var desc = metatron(obj);
	var x = desc.find('arr.1.x');
	x.value = 'modified';
	var newObj = desc.build();
	
	//assert
	
	strictEqual(obj.arr[1].x, 'goal');
	strictEqual(newObj.arr[1].x, 'modified');
});

test('build - undefined', function(){
	//arrange
	var desc = metatron(undefined);
	
	//act
	var test = desc.build();
	
	//assert
	strictEqual(test, undefined);
});

test('build - undefined, include meta is true', function(){
	//arrange
	var desc = metatron(undefined);
	
	//act
	var test = desc.build(true);
	
	//assert
	strictEqual(test, undefined);
});



test('build - null', function(){
	//arrange
	var desc = metatron(null);
	
	//act
	var test = desc.build();
	
	//assert
	strictEqual(test, null);
});

test('build - null, include meta is true', function(){
	//arrange
	var desc = metatron(null);
	
	//act
	var test = desc.build(true);
	
	//assert
	strictEqual(test, null);
});

test('build - string', function(assert){
	//arrange
	var desc = metatron('hi');
	
	//act
	var test = desc.build();
	
	//assert
	strictEqual(test, 'hi');
});

test('build - string, include meta is true', function(assert){
	//arrange
	var desc = metatron('hi');
	
	//act
	var test = desc.build(true);
	
	//assert
	strictEqual(test, 'hi');
});

test('build - number', function(assert){
	//arrange
	var desc = metatron(2);
	
	//act
	var test = desc.build();
	
	//assert
	strictEqual(test, 2);
});

test('build - number, include meta is true', function(assert){
	//arrange
	var desc = metatron(2);
	
	//act
	var test = desc.build(true);
	
	//assert
	strictEqual(test, 2);
});


test('build - boolean', function(assert){
	//arrange
	var desc = metatron(true);
	
	//act
	var test = desc.build();
	
	//assert
	strictEqual(test, true);
});

test('build - boolean, include meta is true', function(assert){
	//arrange
	var desc = metatron(true);
	
	//act
	var test = desc.build(true);
	
	//assert
	strictEqual(test, true);
});

test('build - object', function(assert){
	//arrange
	var orig = {a: 1, b: 'string', c: 3.14, e:{f: 1}, g:[[1], {x: false}]};
	var desc = metatron(orig);
	
	//act
	var test = desc.build();
	
	//assert
	ok(test !== orig);
	deepEqual(test, orig);
});

test('build - object, include meta is true', function(assert){
	//arrange
	var orig = {a: 1, b: 'string', c: 3.14, e:{f: 1}, g:[[1], {x: false}]};
	var desc = metatron(orig);
	
	//act
	var test = desc.build(true);
	
	//assert
	ok(test !== orig);
	deepEqual(test, orig);
});

test('build - array', function(assert){
	//arrange
	var orig = [1, 'string', 3.14, {f: 1}, [[1], [2]]];
	var desc = metatron(orig);
	
	//act
	var test = desc.build();
	
	//assert
	ok(test !== orig);
	deepEqual(test, orig);
});


test('build - array, include meta is true', function(assert){
	//arrange
	var orig = [1, 'string', 3.14, {f: 1}, [[1], [2]]];
	var desc = metatron(orig);
	
	//act
	var test = desc.build(true);
	
	//assert
	ok(test !== orig);
	deepEqual(test, orig);
});

test('build - complex, useMeta is true, built without meta', function(assert){
	//arrange
	var orig = [
			{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}}, //meta object
			{
				a: 2,
				b: {
					c: 4,
					__c : {},
				},
			},
			[ {__0 : {d : 'metaValue'}},  1,2,3]
		];
	var orig_no_meta = 	[
			{
				a: 2,
				b: {
					c: 4
				},
			},
			[1,2,3]
		];
	var desc = metatron(orig, true);
	
	//act
	var test = desc.build();
	
	ok(test !== orig);
	deepEqual(test, orig_no_meta);
});

test('build - complex, useMeta is true, built with meta', function(assert){
	//arrange
	var orig = [
			{__0 : {testMeta: 12}, __1 : {otherMeta : 'metaValue'}}, //meta object
			{
				a: 2,
				b: {
					c: 4,
					__c : true,
				},
			},
			[ {__0 : {d : 'metaValue'}},  1,2,3]
		];
	var desc = metatron(orig, true);
	
	//act
	var test = desc.build(true);
	
	ok(test !== orig);
	deepEqual(test, orig);
});

test('build - complex, useMeta is false, built without meta', function(assert){
	//arrange
	var orig = [
			{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}}, //meta object
			{
				a: 2,
				b: {
					c: 4,
					__c : {},
				},
			},
			[ {__0 : {d : 'metaValue'}},  1,2,3]
		];
	var desc = metatron(orig, false);
	
	//act
	var test = desc.build();
	
	ok(test !== orig);
	deepEqual(test, orig);
});

test('build - complex, useMeta is false, built with meta', function(assert){
	//arrange
	var orig = [
			{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}}, //meta object
			{
				a: 2,
				b: {
					c: 4,
					__c : {},
				},
			},
			[ {__0 : {d : 'metaValue'}},  1,2,3]
		];
	var desc = metatron(orig, false);
	
	//act
	var test = desc.build(true);
	
	ok(test !== orig);
	deepEqual(test, orig);
});

test('find - undefined, null, or empty path returns root object', function(){
	var desc = metatron({});

	//act/assert
	strictEqual(desc.find(undefined), desc);
	strictEqual(desc.find(null), desc);
	strictEqual(desc.find(''), desc);
	strictEqual(desc.find('    '), desc);
	strictEqual(desc.find([]), desc);
});

test('find - path string', function(){
	//arrange
	var obj = { arr: [1, {x: 'goal'}]};
	var desc = metatron(obj);
	
	//act
	var goal = desc.find('arr.1.x');
	
	//assert
	strictEqual(goal.name, 'x');
	strictEqual(goal.value, 'goal');
});

test('find - path string, alternate array syntax', function(){
	//arrange
	var obj = { arr: [1, {x: 'goal'}]};
	var desc = metatron(obj);
	
	//act
	var goal = desc.find('arr[].1.x');
	
	//assert
	strictEqual(goal.name, 'x');
	strictEqual(goal.value, 'goal');
});

test('find - path array', function(){
	//arrange
	var obj = { arr: [1, {x: 'goal'}]};
	var desc = metatron(obj);
	
	//act
	var goal = desc.find(['arr', '1', 'x']);
	
	//assert
	strictEqual(goal.name, 'x');
	strictEqual(goal.value, 'goal');
});

test('find - path array, numbers for indexes', function(){
	//arrange
	var obj = { arr: [1, {x: 'goal'}]};
	var desc = metatron(obj);
	
	//act
	var goal = desc.find(['arr', 1, 'x']);
	
	//assert
	strictEqual(goal.name, 'x');
	strictEqual(goal.value, 'goal');
});

test('find - path array, alternate array syntax', function(){
	//arrange
	var obj = { arr: [1, {x: 'goal'}]};
	var desc = metatron(obj);
	
	//act
	var goal = desc.find(['arr[]', '1', 'x']);
	
	//assert
	strictEqual(goal.name, 'x');
	strictEqual(goal.value, 'goal');
});


test('find - path not found', function(){
	//arrange
	var obj = {};
	var desc = metatron(obj);
	
	//act
	var goal = desc.find('fake.1.x');
	
	//assert
	strictEqual(goal, null);
});

test('set - undefined', function(){
	//act
	var desc = metatron({}).set(undefined);

	//assert
	exists(desc);
	strictEqual(desc.type, 'undefined');
	strictEqual(desc.isUndefined, true);
	strictEqual(desc.isNull, false);
	notExists(desc.value);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});


test('set - null', function(){
	//act
	var desc = metatron({}).set(null);

	//assert
	exists(desc);
	strictEqual(desc.type, 'object');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, true);
	notExists(desc.value);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});

test('set - number', function(){
	//act
	var desc = metatron({}).set(1);

	//assert
	exists(desc);
	strictEqual(desc.type, 'number');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, false);
	strictEqual(desc.value, 1);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});

test('set - boolean', function(){
	//act
	var desc = metatron({}).set(true);

	//assert
	exists(desc);
	strictEqual(desc.type, 'boolean');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, false);
	strictEqual(desc.value, true);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});

test('set - string', function(){
	//act
	var desc = metatron({}).set('hi');

	//assert
	exists(desc);
	strictEqual(desc.type, 'string');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, false);
	strictEqual(desc.value, 'hi');
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.children);
});

test('set - object', function(){
	//arrange
	var obj = {a: 1, b: 'hi', c: true};
	
	//act
	var desc = metatron({}).set(obj);
	
	//assert
	exists(desc);
	strictEqual(desc.type, 'object');
	notExists(desc.name);
	strictEqual(desc.children.length, 3);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	notExists(desc.parent);
	
	var child1 = desc.children[0];
	
	strictEqual(child1.type, 'number');
	strictEqual(child1.name, 'a');
	strictEqual(child1.value, 1);
	deepEqual(child1.path, ['a']);
	notExists(child1.children);
	strictEqual(child1.parent, desc);
	
	var child2 = desc.children[1];
	
	strictEqual(child2.type, 'string');
	strictEqual(child2.name, 'b');
	strictEqual(child2.value, 'hi');
	deepEqual(child2.path, ['b']);
	notExists(child2.children);
	strictEqual(child2.parent, desc);
	
	var child3 = desc.children[2];
	
	strictEqual(child3.type, 'boolean');
	strictEqual(child3.name, 'c');
	strictEqual(child3.value, true);
	deepEqual(child3.path, ['c']);
	notExists(child3.children);
	strictEqual(child3.parent, desc);
});

test('set - object with metadata, useMeta is true', function(){
	//arrange
	var obj = {
		__a : { metaName : 'metaValue' },
		a : true
	};
	
	//act
	var desc = metatron(null, true).set(obj, true);
	
	//assert
	strictEqual(desc.children.length, 1);
	
	var a = desc.children[0];
	strictEqual(a.name, 'a');
	strictEqual(a.meta.metaName, 'metaValue');
});

test('set - object with metadata, useMeta is false', function(){
	//arrange
	var obj = {
		__a : { metaName : 'metaValue' },
		a : true
	};
	
	//act
	var desc = metatron(null, true).set(obj, false);
	
	//assert
	strictEqual(desc.children.length, 2);
});

test('set - array', function(){
	//act
	var desc = metatron(undefined).set([1, 'hi', true]);

	//assert
	exists(desc);
	strictEqual(desc.type, 'array');
	strictEqual(desc.isUndefined, false);
	strictEqual(desc.isNull, false);
	notExists(desc.value);
	notExists(desc.name);
	notExists(desc.parent);
	deepEqual(desc.path, []);
	strictEqual(desc.pathString, '');
	exists(desc.children);
	strictEqual(desc.children.length, 3);
	
	var child1 = desc.children[0];
	
	strictEqual(child1.type, 'number');
	strictEqual(child1.value, 1);
	strictEqual(child1.parent, desc);
	deepEqual(child1.path, ['0']);
	strictEqual(child1.pathString, '0');
	
	var child2 = desc.children[1];
	
	strictEqual(child2.type, 'string');
	strictEqual(child2.value, 'hi');
	strictEqual(child2.parent, desc);
	deepEqual(child2.path, ['1']);
	strictEqual(child2.pathString, '1');
	
	var child3 = desc.children[2];
	
	strictEqual(child3.type, 'boolean');
	strictEqual(child3.value, true);
	strictEqual(child3.parent, desc);
	deepEqual(child3.path, ['2']);
	strictEqual(child3.pathString, '2');
});


test('set - array metadata, useMeta is true', function(){
	//arrange
	var obj = [
			{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}}, //meta object
			{a: 2},
			{b: 4},
			[1,2,3]
		];
	
	//act
	var desc = metatron('hi', true).set(obj);
	
	//assert
	exists(desc);
	equal(desc.children.length, 3);

	var firstElement = desc.find('0');
	exists(firstElement);
	equal(firstElement.meta.testMeta, 12);
	notExists(firstElement.meta.otherMeta);

	var secondElement = desc.find('1');
	exists(secondElement);
	notExists(secondElement.meta);
	
	var thirdElement = desc.find('2');
	exists(thirdElement);
	equal(thirdElement.meta.otherMeta, 'metaValue');
	notExists(thirdElement.meta.testMeta);
});

test('set - array metadata, useMeta is false', function(){
	//arrange
	var obj = [
			{__0 : {testMeta: 12}, __2 : {otherMeta : 'metaValue'}}, //meta object
			{a: 2},
			{b: 4},
			[1,2,3]
		];
	
	//act
	var desc = metatron('hi', true).set(obj, false);
	
	//assert
	exists(desc);
	equal(desc.children.length, 4);
});

test('set - set to existing MetatronDescriptor uses copy of descriptor', function(){
	//arrange
	var obj = {x: 1};
	var desc = metatron(obj);
	var test = metatron(null);
	
	//act
	test.set(desc);
	
	//assert
	ok(desc !== test);
	deepEqual(desc, test);
});

test('set - set to existing MetatronDescriptor uses copy of descriptor, ignores meta data when useMeta is false', function(){
	//arrange
	var obj = {__x: true, x: 1};
	var desc = metatron(obj, true);
	var test = metatron(null, true);
	
	//act
	test.set(desc, false);
	
	//assert
	ok(desc !== test);
	strictEqual(test.find('x').meta, undefined);
});


test('set - setting value of nested object', function(){
	//arrange
	var obj = {__x: true, x: 1};
	var desc = metatron(obj);
	var test = desc.find('x');
	
	//act
	test.set({__a: true, a : 2});
	
	//assert
	deepEqual(desc.build(true), {
		__x : true,
		x: {
			__a : true,
			a: 2
		}
	});
});

test('set - metaData arg not given, old metaData is copied over', function(){
	//arrange
	var obj = {__x: true, x: 1};
	var desc = metatron(obj);
	var test = desc.find('x');
	
	//act
	test.set(2);
	
	//assert
	strictEqual(test.meta, true);
	strictEqual(test.value, 2);
});

test('setPath - path is empty affects current descriptor, useMeta is true', function(){
	//arrange
	var obj1 = { __x : true, x : 1 };
	var obj2 = { __a : false, a : 2 };
	var obj2_noMeta = { a : 2 };
	
	//act/assert
	deepEqual(metatron(obj1).setPath(undefined, obj2).build(false), obj2_noMeta); //useMeta default is true
	deepEqual(metatron(obj1).setPath("", obj2).build(false), obj2_noMeta); //useMeta default is true
	deepEqual(metatron(obj1).setPath("  ", obj2, true).build(false), obj2_noMeta);
	deepEqual(metatron(obj1).setPath([], obj2, true).build(false), obj2_noMeta);
});

test('setPath - path is empty affects current descriptor, useMeta is false', function(){
	//arrange
	var obj1 = { __x : true, x : 1 };
	var obj2 = { __a : false, a : 2 };
	
	//act/assert
	deepEqual(metatron(obj1).setPath(undefined, obj2, false).build(false), obj2);
	deepEqual(metatron(obj1).setPath("", obj2, false).build(false), obj2);
	deepEqual(metatron(obj1).setPath("  ", obj2, false).build(false), obj2);
	deepEqual(metatron(obj1).setPath([], obj2, false).build(false), obj2);
});

test('setPath - finds existing path and updates value', function(){
	//arrange
	var obj = { __x : true, x : 1 };
	var updatedValue = 2;
	var desc = metatron(obj);
	
	//act
	desc.setPath('x', updatedValue);
	
	//assert
	deepEqual(desc.build(true), { __x : true, x : 2 });
});

test('setPath - creates non-existent path and updates value', function(){
	//arrange
	var obj = {};
	var updatedValue = { __f : true, f: 'hi' };
	var desc = metatron(obj);
	
	//act
	desc.setPath('a.b[].1.d.e', updatedValue);
	
	//assert
	deepEqual(desc.build(true), {
			a : {
				b : [
					undefined,
					{
						d: 
							{
								e: {
									__f : true,
									f : 'hi'
								}
							}
					}
				]
			}
	});
});

test('setPath - other paths not affected', function(){
	//arrange
	var obj = [ { a : true }, { b : { c : 1}, d : 2 } ];
	var desc = metatron(obj);
	
	//act
	desc.setPath([1, 'b', 'c'], 3);
	
	//assert
	deepEqual(desc.build(),  [ { a : true }, { b : { c : 3}, d : 2} ]);
});

test('setPath - returns same node as used for the method call', function(){
	//arrange
	var obj = [ ];
	var desc = metatron(obj);
	
	//act
	var result = desc.setPath([1], 3);
	
	//assert
	strictEqual(result, desc);
	deepEqual(desc.build(),  [undefined, 3]);
});

test('setPath - non object converted to object, meta data is retained', function(){
	//arrange
	var obj = { __a: 'hi', a: undefined };
	var updatedValue = { __f : true, f: 'hi' };
	var desc = metatron(obj);
	
	//act
	desc.setPath('a', updatedValue);
	
	//assert
	deepEqual(desc.build(true), {
			__a : 'hi',
			a : {
				__f : true,
				f : 'hi'
			}
	});
});

test('setPath - non array converted to array, meta data is retained', function(){
	//arrange
	var obj = { __a: 'hi', a: {} };
	var updatedValue = { __f : true, f: 'hi' };
	var desc = metatron(obj);
	
	//act
	desc.setPath('a[].0', updatedValue);
	
	//assert
	deepEqual(desc.build(true), {
			__a: 'hi',
			a : [
				{
					__f : true,
					f : 'hi'
				}
			]
	});
});

test('setPath - useMeta is false', function(){
	//arrange
	var obj = { a: {} };
	var updatedValue = [ { __f : true, f: 'hi' } ];
	var desc = metatron(obj);
	
	//act
	desc.setPath('a', updatedValue, false);
	
	//assert
	deepEqual(desc.build(false), {
			a : [
				{
					__f : true, //this is interpreted as a regular field
					f : 'hi'
				}
			]
	});
});

test('setPath - pathStrings interpreted consistently', function(){
	//arrange
	var obj = [ { a : { b : [{ c : 1}] } }];
	var desc = metatron(obj);
	var copy = metatron(); //empty metatron object
	var cNode = desc.find('0.a.b[].0.c');
	
	//act
	copy.setPath(cNode.pathString, cNode.value);
	
	//assert
	deepEqual(copy.build(), desc.build());
});

test('remove - no parent does nothing', function(){
	//arrange
	var desc = metatron({});
	
	//act
	var result = desc.remove();
	
	//assert
	strictEqual(result, desc);
	equal(desc.parent, null);
});

test('remove - removes child from parent not affecting other children', function(){
	//arrange
	var parent = metatron({ a : true, b : false, c : true });
	var child = parent.find('b');
	
	//act
	child.remove();
	
	//assert
	equal(child.parent, null);
	strictEqual(parent.children.length, 2);
	strictEqual(parent.children[0].name, 'a');
	strictEqual(parent.children[1].name, 'c');
});

test('removePath - object path is found', function(){
	//arrange
	var obj = [ { a : { b : [{ c : 1}] } }];
	var desc = metatron(obj);
	var cNode = desc.find('0.a.b[].0.c');
	
	//act
	var result = desc.removePath('0.a.b[].0.c');
	
	//assert
	strictEqual(result, cNode);
	deepEqual(desc.build(), [ { a : { b : [{}] } }]);
});

test('removePath - array path is found', function(){
	//arrange
	var obj = [ { a : { b : [ 0, 1 ] } }];
	var desc = metatron(obj);
	var node = desc.find('0.a.b[].1');
	
	//act
	var result = desc.removePath('0.a.b[].1');
	
	//assert
	strictEqual(result, node);
	deepEqual(desc.build(), [ { a : { b : [0] } }]);
});

test('removePath - path not found, does nothing', function(){
	//arrange
	var obj = [ { a : { b : [{ c : 1}] } }];
	var desc = metatron(obj);
	
	//act
	var result = desc.removePath('0.a.b[].0.d');
	
	//assert
	strictEqual(result, null);
	deepEqual(desc.build(),  [ { a : { b : [{ c : 1}] } }]);
});

test('getMeta - meta object is created when it does not exist', function(){
	//arrange
	var desc = metatron();
	desc.meta = null;
	
	//act
	var meta = desc.getMeta();
	
	//assert
	deepEqual(meta, {});
	strictEqual(meta, desc.meta);
});

test('getMeta - meta object not created when already exists, meta is object', function(){
	//arrange
	var desc = metatron();
	desc.meta = {a: 1};
	
	//act
	var meta = desc.getMeta();
	
	//assert
	deepEqual(meta, {a: 1});
	strictEqual(meta, desc.meta);
});

test('getMeta - meta object not created when already exists, meta is non-object', function(){
	//arrange
	var desc = metatron();
	desc.meta = false;
	
	//act
	var meta = desc.getMeta();
	
	//assert
	strictEqual(meta, false);
	strictEqual(meta, desc.meta);
});

test('filter - remove all', function(){
	//arrange
	var obj = [{a:1, b:{c: 1, d: [4]}}, 4, 3];
	var desc = metatron(obj);
	
	//act
	desc.filter(function(){ return false; });
	
	//assert
	deepEqual(desc.build(), []);
});

test('filter - keep all', function(){
	//arrange
	var obj = [{a:1, b:{c: 1, d: [4]}}, 4, 3];
	var desc = metatron(obj);
	
	//act
	desc.filter(function(){ return true; });
	
	//assert
	deepEqual(desc.build(), [{a:1, b:{c: 1, d: [4]}}, 4, 3]);
});

test('filter - removes objects that match and leaves those that do not', function(){
	//arrange
	var obj = [{a:1, b:{c: 1, d: [4]}}, 4, 3];
	var desc = metatron(obj);
	
	//act
	desc.filter(function(){
		return this.type == 'array' || this.type =='object' || this.value == 4;
	});
	
	//assert
	deepEqual(desc.build(), [{b:{d: [4]}}, 4]);
});

test('filter - does not affect current node', function(){
	//arrange
	var obj = { a : {b : 1 } };
	var desc  = metatron(obj);
	var node = desc.find('a');
	
	//act
	node.filter(function(){return false; });
	
	//assert
	deepEqual(desc.build(), { a : { } });
});

test('filter - use meta data', function(){
	//arrange
	var obj = {
		__a: {required : true}, a:1, 
		__b : {required:false}, b:2 
	};
	var desc = metatron(obj);
	
	//act
	desc.filter(function(){
		return this.meta && this.meta.required;
	});
	
	//assert
	deepEqual(desc.build(), {a:1});
});

test('constructor property', function(){
	//arrange
	var meta = metatron();
	var obj = {};
	
	//act/assert
	strictEqual(meta.constructor, metatron);
	ok(meta instanceof metatron);
	ok(meta instanceof Object);
	ok(!(obj instanceof metatron));
	ok(obj instanceof Object);
});